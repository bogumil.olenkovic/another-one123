patiekalu_saraso_failas = "saraso pavyzdys.txt"
print("Iveskite patiekalo pavadinima:")
patiekalas = (input()).strip()

exists = False
with open(patiekalu_saraso_failas, "r") as sarasas:
    for line in sarasas:
        line = line.strip()
        if line != "" and patiekalas == line:
            exists = True
            break


if exists:
    print("Patiekalas jau egzistuoja. Prideti negalima.")
else:
    print("Tokio patiekalo dar nera.\nAr tikrai norite prideti patiekala i sarasa? (T/N)")
    yesno = input().lower()
    if yesno == "t":
        with open(patiekalu_saraso_failas, "a") as sarasas:
            sarasas.write(patiekalas + "\n")
        print("Patiekalas sekmingai pridetas i sarasa.")
    if yesno == "n":
        print("Patiekalas nepridetas i sarasa.")
    