def verify_password(password):
    for i in password:
        i = ord(i)
        if not (48 <= i <= 57 or 65 <= i <= 90 or 97 <= i <= 122):
            return False
    return True


def encrypt(password, key):
    return "".join([chr(ord(i) - key) for i in password])


def decrypt(password, key):
    return "".join([chr(ord(i) + key) for i in password])


print("Pateikite slaptazodi:")
password = input()


if(verify_password(password)):
    print("\nSlaptazodis tinkamas.\n")
    key = 5
    encrypted_password = encrypt(password, key)
    print("Slaptazodis po sifravimo:\n%s\n" % encrypted_password)
    decrypted_password = decrypt(encrypted_password, key)
    print("Sifruotas slaptazodis po issifravimo:\n%s" % decrypted_password)
else:
    print("\nSlaptazodis netinkamas.")